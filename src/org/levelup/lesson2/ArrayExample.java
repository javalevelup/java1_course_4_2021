package org.levelup.lesson2;

public class ArrayExample {

    public static void main(String[] args) {

        double price1 = 6453.34;
        double price2 = 5634.32;
        double price3 = 6764.75;
        double price4 = 8764.64;
        double price5 = 1264.45;

        // if (productNumber == 1) sout(price1); else if (productNumber == 2) sout(price2);

        double[] prices = new double[5]; // Массив типа double
        System.out.println(prices[3]);
        prices[0] = price1;
        prices[1] = price2;
        prices[2] = price3;
        prices[3] = price4;
        prices[4] = price5;

        System.out.println(prices[3]);
        System.out.println();

        double sum = 0;
        for (int index = 0; index < prices.length; index++) {
            System.out.println(prices[index]);
            sum = sum + prices[index];
        }

        double average = sum / prices.length;
        System.out.println(average);

    }

}
