package org.levelup.lesson2;

public class Loops {

    public static void main(String[] args) {

        // for (;;) {}
        // while (true) {}
        // do { } while (true);

        for (int i = 0;; i++) {
            int res = (int) Math.pow(2, i);
            System.out.println(res);
            if (res == 2147483647) {
                System.out.println("Конец цикла");
                break;
            }
        }

        for (int i = 0; i < 11; i++) {
            int result = (int) Math.pow(2, i); // 2^i
            System.out.println(result);
        }

        int result = 1;
        while (result < 1000) {
            System.out.println(result);
            result = result * 3;
        }

        System.out.println();
        int res = 1;
        do {
            System.out.println(res);
            res = res * 4;
        } while (res < 1000);

    }

}
