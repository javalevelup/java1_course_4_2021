package org.levelup.lesson2;

import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число:");
        int number = sc.nextInt();

        // Псевдо-случайные числа (ПСЧ)
        Random r = new Random();
        int secretNumber = r.nextInt(4); // [0, 4) -> [0, 3]
        // [-5, 5]
        // ... = r.nextInt(11) - 5; // -> [0, 10] - 5 -> [-5, 5]

//        if (number == secretNumber) {
//            System.out.println("Вы угадали!");
//        } else {
//            if (number > secretNumber) {
//                System.out.println("Вы ввели число большее чем сгенерированное");
//            } else {
//                System.out.println("Вы ввели число меньшее чем сгенерированное");
//            }
//        }

        if (number == secretNumber) {
            System.out.println("Вы угадали!");
        } else if (number > secretNumber) {
            System.out.println("Вы ввели число, большее чем сгенерированное");
            System.out.println("Загаданное число " + number);
        } else {
            System.out.println("Вы ввели число, меньшее чем сгенерированное");
            // int variable = 5;
        }
        // variable = 10;


    }

}
