package org.levelup.lesson2;

@SuppressWarnings("ALL")
public class ArithmeticOperations {

    public static void main(String[] args) {
        //
        int first = 17;
        int second = 4;

        int third = first / second; // 15 / 4 = 3.75
        System.out.println(third);

        long longVar = third;       // int -> long
        double doubleVar = longVar; // long -> double
        double dVar = third;        // int -> double

        int intVar = (int) doubleVar;     // double -> int

        int i = 130;
        byte byteVar = (byte) i;        // [-128, 127]
        // 128 -> -128
        // 129 -> -127
        // 130 -> -126
        // -129 -> 127
        System.out.println(byteVar);    // -126
        System.out.println((byte) 450);

        // double + <float/long/int/short/byte> -> double
        // float + <long/int> -> float
        // long + int -> long
        // int + int -> int

        double res = first / second; // (double) (int / int)
        System.out.println(res);

        System.out.println(+'c');

        int value = 10;
        // increment - увеличение значения на 1
        // decrement - уменьшение значения на 1
        value++;    // value = value + 1;
        value--;    // value = value - 1;
        //
        // value++; - postfix increment - постфиксный инкремент
        // ++value; - prefix increment - префиксный инкремент

        int result = 10 * value++; // 1. Сначала все операции в выражении. 2. Икремент
        // value = 10;
        int result2 = 10 * ++value; // 1. Сначала инкремент. 2. Все оставшиеся операции в выражении

        // 1.
        // result = 10 * value;
        // value = value + 1;

        // 2.
        // value = value + 1;
        // result = 10 * value;

        boolean r = first != second;

    }

}
