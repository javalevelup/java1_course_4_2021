package org.levelup.lesson5.comparison.test;

public class B extends A {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        // if (!(o instanceof B)) return false;
        if (o == null || getClass() != o.getClass()) return false;

        B other = (B) o;
        return fieldA == other.fieldA;
    }

}
