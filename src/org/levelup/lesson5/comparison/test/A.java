package org.levelup.lesson5.comparison.test;

public class A {

    int fieldA;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        // if (!(o instanceof A)) return false;
        if (o == null || getClass() != o.getClass()) return false;

        A other = (A) o;
        return fieldA == other.fieldA;
    }

}
