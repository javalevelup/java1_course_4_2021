package org.levelup.lesson5.comparison.test;

public class ABTest {

    public static void main(String[] args) {
        A a = new A();
        a.fieldA = 10;

        B b = new B();
        b.fieldA = 10;

        System.out.println(a.equals(b));
        System.out.println(b.equals(a));

    }

}
