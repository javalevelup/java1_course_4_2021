package org.levelup.lesson5.comparison;

import java.util.Objects;

public class Phone {

    private String model;
    private int width;
    private int length;

    public Phone(String model, int width, int length) {
        this.model = model;
        this.width = width;
        this.length = length;
    }

    @Override
    // obj -> secondPhone
    public boolean equals(Object obj) {
        // this - первый объект из сравнения (тот объект, у которого вызвали метод equals())
        // object - второй объект из сравнения (тот объект, который передали в качестве аргументва метода equals())
        // this.model ~ model
        //
        if (this == obj) return true;
        // Проверку на тип
        // 2 варианта проверки типов

        // 1 вариант
        // Использование оператора instanceof
        // <переменная> instanceof <Класс>

        // obj instanceof Phone -> true, только если obj имеет тип Phone
        // null instanceof Phone -> false всегда
        // if (!(obj instanceof Phone)) return false;

        // 2 вариант
        if (obj == null || getClass() != obj.getClass()) return false;
        // Проверку на nullы
        Phone other = (Phone) obj;
        return width == other.width
                && Objects.equals(model, other.model);
                // && (model == other.model || (model !=null && model.equals(other.model)));
                // && model.equals(other.model);
    }

    // 5
    // a -> e
    // e -> a
    // 53454 -> ab33abc35ac3
    @Override
    public int hashCode() {
//        int result = 31;
//        result = 31 * result + width;
//        result = 31 * result + model.hashCode();
//        return result;
        return Objects.hash(width, model);
    }

    // getters/setters

    // getters - методы, которые позволяют получить значение поля класса
    // public <тип поля> get<Имя поля>() { return <поле>;}
    public String getModel() {
        return model;
    }

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }

    // setters - методы, которые позволяют установить значение поля
    // public void set<Имя поля>(<Тип поля> <имя поля>) { this.<Имя поля> = <Имя поля>; }
    public void setModel(String model) {
        this.model = model;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setWidth(int width) {
        this.width = width;
    }

}
