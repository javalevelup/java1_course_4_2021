package org.levelup.lesson5.comparison;

@SuppressWarnings("ALL")
public class PhoneApp {

    public static void main(String[] args) {

        Phone firstPhone = new Phone("Sony", 5, 11);
        Phone secondPhone = new Phone("Sony", 5, 10);
        System.out.println(firstPhone.getModel() + " " + firstPhone.getLength() + " " + firstPhone.getWidth());

        Phone thirdPhone = firstPhone;
//        thirdPhone.setModel("LG");
//        System.out.println(firstPhone.getModel());
        boolean isEqual = firstPhone == secondPhone; // false
        boolean equals = firstPhone.equals(secondPhone); // secondPhone -> (Object) secondPhone;

        System.out.println("Equals through method: " + equals);

        // firstPhone.equals("Some string"); // -> ClassCastException
        // firstPhone.equals(null);

        // сlass A {} A a = new A();
        // class B extends A {}, B b = new B();

        // a.equals(b); -> true
        // b.equals(a); -> false

        Phone[] phones = new Phone[] {
                new Phone("Sony", 5, 10),
                new Phone("Sony", 6, 10),
                new Phone("Samsung", 8, 10)
        };

        Phone f = new Phone("Sony", 6, 10);
        Phone s = new Phone("LG", 4, 9);

        int sonyIndex = indexOf(phones, f);
        int lgIndex = indexOf(phones, s);

        System.out.println("Sony's index: " + sonyIndex);
        System.out.println("LG's index: " + lgIndex);
    }

    // возвращает индекс элемента searchValue из массива Phone
    static int indexOf(Phone[] phones, Phone searchValue) {
        int searchValueHashCode = searchValue.hashCode();
        for (int i = 0; i < phones.length; i++) {
            if (searchValueHashCode == phones[i].hashCode() && searchValue.equals(phones[i])) {
                return i;
            }
        }
        return -1;
    }

}
