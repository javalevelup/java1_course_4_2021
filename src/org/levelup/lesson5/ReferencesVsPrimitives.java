package org.levelup.lesson5;

import org.levelup.lesson3.Person;

@SuppressWarnings("ALL")
public class ReferencesVsPrimitives {

    public static void main(String[] args) {
        int primitive = 10;
        Person person = new Person();
        person.age = 10;

        // primitive = methodWithPrimitive2(primitive);
        methodWithPrimitive(primitive);
        methodWithReference(person);

        System.out.println(primitive);      // 20
        System.out.println(person.age);     // 10?20
    }

    static int methodWithPrimitive2(int value) {
        value = 20;
        return value;
    }

    static void methodWithPrimitive(int value) {
        value = 20;
    }

    static void methodWithReference(Person reference) {
        reference.age = 20;
    }

    static void methodWithReference2(Person reference) {
        reference = new Person();
        reference.age = 20;
    }

}
