package org.levelup.lesson8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Maths {

    // модуль числа
    public static double abs(double a) {
        // double var = <condition> ? <true statement> : <false statement>
//        if (a < 0) {
//            return -a;
//        }
//        return a;
        return a < 0 ? -a : a;
    }

    public static List<Integer> get(List<Integer> original) {
        return original == null || original.isEmpty() ? new ArrayList<>() : original;
    }

}
