package org.levelup.lesson8;

import org.levelup.lesson7.LinkedIntList;

import java.util.Iterator;

public class IteratorExample {

    public static void main(String[] args) {
        LinkedIntList lil = new LinkedIntList();
        lil.addLast(4);
        lil.addLast(45);
        lil.addLast(55);
        lil.addLast(65);

        for (int val : lil) {
            System.out.println(val);
        }

        Iterator<Integer> iter = lil.iterator();
        // O(n)
        while (iter.hasNext()) {
            Integer val = iter.next();
            System.out.println(val);
        }

    }

}
