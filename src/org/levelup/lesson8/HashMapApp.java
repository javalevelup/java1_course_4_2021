package org.levelup.lesson8;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HashMapApp {

    public static void main(String[] args) {
        Map<String, Double> productPrices = new HashMap<>();

        productPrices.put("Молоко", 67.4);
        productPrices.put("Сыр", 564.3);
        productPrices.put("Чай", 167.64);
        productPrices.put("Кофе", 343.4);

        Double cheesePrice = productPrices.get("Сыр");
        System.out.println(cheesePrice);
        System.out.println(productPrices.get("Шоколад"));

        // 1 variant
        Set<String> keys = productPrices.keySet(); // список всех ключей, которые есть в Map
        for (String key : keys) { // O(n)
            System.out.println(key + " " + productPrices.get(key));
        }

        System.out.println();

        productPrices.put("Сыр", 678.5);

        // 2 variant
        Set<Map.Entry<String, Double>> entries = productPrices.entrySet(); // список всех пар
        for (Map.Entry<String, Double> entry : entries ) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        System.out.println();
        System.out.println(productPrices.values());

        System.out.println();
        Map<String, List<Double>> map = new HashMap<>();
        Map<String, Map<String, Map<String, Integer>>> hellMap = new HashMap<>();

        Collection<Map<String, List<Integer>>> wtf = new ArrayList<>();

    }

}
