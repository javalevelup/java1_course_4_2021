package org.levelup.lesson8;

@SuppressWarnings("ALL")
public class WrapperExample {

    public static void main(String[] args) {

        Integer i6; // vs int
        Long l1;
        Double d1;

        // Flyweight
        // boxing: primitive -> Wrapper class
        // unboxing: wrapper class -> primitive

        Integer integer = Integer.valueOf(10);
        int intValue = integer.intValue();

        Integer integer2 = 50; // autoboxing
        int intValue2 = integer2; // autounboxing


        Integer i1 = 127; // Integer.valueOf(127) -> new Integer(127)
        Integer i2 = 127;
        Integer i3 = 129;
        Integer i4 = 129;

        System.out.println(i1 == i2); // true
        System.out.println(i3 == i4); // false

    }

}
