package org.levelup.lesson8;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("ALL")
public class Iterations {

    public static void main(String[] args) {
        //
        List<Integer> list = new LinkedList<>();

        // O(?) - печать элементов на экран
        // ArrayList - O(n)
        // LinkedList - O(n*n)

        // for (0..n) - O(n)
        // for (0..n) { for(0..n) } -> O(n*n)
        // for (0..n) { for(0..n) { for (0..n) } } -> O(n*n*n)
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        for (Integer val : list) {
            System.out.println(val);
        }

//        Iterator<Integer> iter = list.iterator();
//        while (iter.hasNext()) {
//            System.out.println(iter.next());
//        }

    }

}
