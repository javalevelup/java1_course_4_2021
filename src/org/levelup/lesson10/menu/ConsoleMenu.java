package org.levelup.lesson10.menu;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ConsoleMenu {

    private static final BufferedReader CONSOLE_READER = new BufferedReader(new InputStreamReader(System.in));

    public void printMenu() {
        System.out.println("1. Get list of all universities");
        System.out.println("0. Exit program");
        // System.out.printf("Print: %s", "weqr");
    }

    /**
     * Read text from console and transform to int value
     *
     * @return command number
     */
    public int enterCommand() throws Exception {
        System.out.println("Enter command number:");
        String command = CONSOLE_READER.readLine();
        return Integer.parseInt(command); // throw new NumberFormatException: "qw3rq2" -> number
    }

}
