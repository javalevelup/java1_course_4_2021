package org.levelup.lesson10.menu.command;

import org.levelup.lesson10.menu.Command;
import org.levelup.lesson10.model.University;
import org.levelup.lesson10.service.FileService;
import org.levelup.lesson10.service.UniversityService;

import java.util.List;

public class PrintUniversitiesCommand implements Command {

    private FileService fileService;
    private UniversityService universityService;

    public PrintUniversitiesCommand() {
        this.fileService = new FileService();
        this.universityService = new UniversityService(fileService);
    }

    @Override
    public void executeCommand() {
        List<University> universities = universityService.getAllUniversities();
        for (University u : universities) {
            System.out.println(u.getFullName() + " (" + u.getShortName() + ")");
        }
    }

}
