package org.levelup.lesson10.menu;

public interface Command {

    void executeCommand();

}
