package org.levelup.lesson10.menu;

import org.levelup.lesson10.menu.command.PrintUniversitiesCommand;

import java.util.HashMap;
import java.util.Map;

// Класс, который хранит соотвествие номера команды (число, которое вводит пользователь) и самой команды (объект интерфейса Command)
public class CommandFactory {

    private static Map<Integer, Command> commands = new HashMap<>();

    // статический блок инициализации
    static {
        commands.put(1, new PrintUniversitiesCommand());
    }

    public static Command findCommand(int command) {
        return commands.get(command);
    }

}
