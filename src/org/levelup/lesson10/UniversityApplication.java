package org.levelup.lesson10;

import org.levelup.lesson10.menu.Command;
import org.levelup.lesson10.menu.CommandFactory;
import org.levelup.lesson10.menu.ConsoleMenu;

public class UniversityApplication {

    public static void main(String[] args) {
        System.out.println("University database management program");

        ConsoleMenu consoleMenu = new ConsoleMenu();
        consoleMenu.printMenu();

        int commandNumber;
        do {
            try {
                commandNumber = consoleMenu.enterCommand();
                Command command = CommandFactory.findCommand(commandNumber);
                if (command != null) {
                    command.executeCommand();
                } else {
                    System.out.println("No command by this value");
                }
            } catch (Exception exc) {
                System.out.println("You entered the invalid command");
                commandNumber = -1;
            }
        } while (commandNumber != 0);

        System.out.println("Goodbye!");

    }

}
