package org.levelup.lesson10.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for reading files and writing to files
 */
public class FileService {

    /**
     * Returns list of lines
     * @param filename file to read
     */
    public List<String> readCsvFile(String filename) {
        System.out.println("Start to reading file: " + filename);

//        BufferedReader r = null;
//        try {
//            r = new BufferedReader(new FileReader(filename));
//            // read file
//            String line;
//            while ( (line = r.readLine()) != null ) { // 1. в line записываем значение, которое считали 2. Значение в line сравниваем с null
//                System.out.println(line);
//            }
//        } catch (IOException exc) {
//            System.out.println(exc.getMessage());
//        } finally {
//            if (r != null) {
//                try {
//                    r.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }

        // try-with-resources
        List<String> lines = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line;
            while ( (line = reader.readLine()) != null ) { // 1. в line записываем значение, которое считали 2. Значение в line сравниваем с null
                if (!line.isBlank()) { // не пустая или не состоит из одних пробелов
                    lines.add(line);
                }
            }

            System.out.println("Finished CSV file reading");
            return lines;
        } catch (IOException exc) {
            System.out.println("Couldn't read file " + filename + ". Message: " + exc.getMessage());
            throw new RuntimeException(exc);
        }

    }

    public void writeToCsvFile(String filename, List<String> lines) {
        System.out.println("Start to writing to file: " + filename);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename, true))) {
            for (String line: lines) {
                writer.newLine();
                writer.write(line);
            }
            writer.flush();
            System.out.println("Finished writing to CSV file");
        } catch (IOException exc) {
            System.out.println("Couldn't write to file " + filename + ". Message: " + exc.getMessage());
            throw new RuntimeException(exc);
        }
    }

}
