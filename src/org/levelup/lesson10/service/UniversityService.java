package org.levelup.lesson10.service;

import org.levelup.lesson10.model.University;

import java.util.ArrayList;
import java.util.List;

public class UniversityService {

    private static final String UNIVERSITIES_FILEPATH = "db/universities.txt";

    private FileService fs;

    public UniversityService(FileService fs) {
        this.fs = fs;
    }

    public List<University> getAllUniversities() {
        List<String> lines = fs.readCsvFile(UNIVERSITIES_FILEPATH);
        System.out.println("Read " + lines.size() + " lines from file");

        List<University> universities = new ArrayList<>(lines.size());
        for (String line : lines) {
            University u = parseCsvLine(line);
            universities.add(u);
        }

        return universities;
    }

    private University parseCsvLine(String line) {
        // 16834,СПбГУ,Государственный университет,1805 -> [16834, СПбГУ, Государственный университет, 1805]
        String[] fields = line.split(",");

        // "12534" -> 12534
        long id = Long.parseLong(fields[0]);
        String shortName = fields[1];
        String fullName = fields[2];
        int foundationYear = Integer.parseInt(fields[3]);

        return new University(id, shortName, fullName, foundationYear);
    }

}
