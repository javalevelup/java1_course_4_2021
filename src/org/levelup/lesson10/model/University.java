package org.levelup.lesson10.model;

public class University {

    // некий уникальный номер университета
    private long id;
    private String shortName;
    private String fullName;
    private int foundationYear;

    public University() {}

    public University(long id, String shortName, String fullName, int foundationYear) {
        this.id = id;
        this.shortName = shortName;
        this.fullName = fullName;
        this.foundationYear = foundationYear;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getFoundationYear() {
        return foundationYear;
    }

    public void setFoundationYear(int foundationYear) {
        this.foundationYear = foundationYear;
    }
}
