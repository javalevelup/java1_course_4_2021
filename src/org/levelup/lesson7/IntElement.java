package org.levelup.lesson7;

// Это класс, который представляет(описывает) элементы списка
// Объект класса IntElement - это и есть элемент списка
public class IntElement {

    private IntElement next;
    // private IntElement previous;
    private int value;

    public IntElement(int value) {
        this.next = null;
        this.value = value;
    }

    public IntElement getNext() {
        return next;
    }

    public void setNext(IntElement next) {
        this.next = next;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

}
