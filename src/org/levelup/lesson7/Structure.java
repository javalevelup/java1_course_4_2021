package org.levelup.lesson7;

public class Structure {

    private Object value;

    public void add(Object value) {
        this.value = value;
    }

    public Object get() {
        return value;
    }

}
