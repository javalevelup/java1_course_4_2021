package org.levelup.lesson7;

import org.levelup.lesson6.IntList;

import java.util.Iterator;

public class LinkedIntList extends IntList implements Iterable<Integer> {

    private IntElement head; // начало нашего списка (первый элемент списка)

    @Override
    public void addFirst(int value) {
        IntElement el = new IntElement(value);
        if (head == null) {
            head = el;
        } else {
            el.setNext(head);
            head = el;
        }
        size++;
//        if (head != null) {
//            el.setNext(head);
//        }
//        head = el;
//        size++;
    }

    @Override
    public void addLast(int value) {
        IntElement el = new IntElement(value); // новый элемент нашего списка
        if (head == null) { // список пуст
            head = el;
        } else {
            // когда в списке есть значения (список не пустой)
            IntElement cursor = head;
            // cursor = cursor.getNext();
            // 1 step: cursor.getNext() -> c5
            // 2 step: cursor = c5;
            while (cursor.getNext() != null) { // cursor.getNext() == 0 -> true - это означает, что мы нашли конец списка
                cursor = cursor.getNext();
            }
            // cursor - указывает на последний элемент списка
            // с7.next = c8 -> cursor.next = el;
            cursor.setNext(el);
        }
        size++;
    }

    public int getFirst() {
        // head maybe null
        if (head == null) {
            return 0;
        }
        return head.getValue();
    }

    @Override
    public void removeByValue(int value) {

    }

    @Override
    public void print() {

    }

    @Override
    public Iterator<Integer> iterator() {
        return new LinkedIntListIterator(head);
    }

}
