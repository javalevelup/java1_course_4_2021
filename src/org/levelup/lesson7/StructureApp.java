package org.levelup.lesson7;

public class StructureApp {

    public static void main(String[] args) {
        Structure structure = new Structure();

        structure.add("Some string");
        // ...
        // structure.add(56);
        // ...
        String s = (String) structure.get();
        System.out.println(s);


        GenericStructure<String> stringGC = new GenericStructure<>();
        stringGC.add("Only strings");
        String s1 = stringGC.get();
        System.out.println(s1);

        GenericStructure<Integer> intGC = new GenericStructure<>();
        intGC.add(56);
        // intGC.add("");
        System.out.println(intGC.get());

    }

}
