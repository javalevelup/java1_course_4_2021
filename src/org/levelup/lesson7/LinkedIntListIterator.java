package org.levelup.lesson7;

import java.util.Iterator;

public class LinkedIntListIterator implements Iterator<Integer> {

    private IntElement current; // курсор

    public LinkedIntListIterator(IntElement head) {
        current = head;
    }

    // [..., 5, 7]
    // 5 -> hasNext() -> true, .next() -> 7
    // 7 -> hasNext() -> true, .next() -> null
    // null -> hasNext() -> false
    @Override
    public boolean hasNext() {
        return current != null;
    }

    // 1. Получить значение текущего элемента
    // 2. Перейти на следующий элемент
    @Override
    public Integer next() {
        int val = current.getValue();
        current = current.getNext();
        return val;
    }

}
