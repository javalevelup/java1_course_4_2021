package org.levelup.lesson7;

import java.util.ArrayList;
import java.util.List;

public class CollectionExample {

    public static void main(String[] args) {

        List<String> productNames = new ArrayList<>(); // список на основе массива

        productNames.add("Конфеты");
        productNames.add("Молоко");
        productNames.add("Сыр");
        productNames.add("Гречка");
        productNames.add("Чай");
        productNames.add("Кофе");

        String fourthElement = productNames.get(3); // получение значение элемента по индексу
        System.out.println(fourthElement);

        boolean isCheese2Exist = productNames.contains("Сыр2");
        System.out.println(isCheese2Exist);

        productNames.remove("Конфеты");
        productNames.remove(0);

        // [5, 6, 7, 8, 9].removeByValue(6); size = 5
        // [5, 7, 8, 9, 9], size = 4

        System.out.println("Размер коллекции: " + productNames.size());
        // foreach
        // for (<Generic type of collection> <var> : <object of collection>)
        for (String name : productNames) {
            System.out.println(name);
        }

        System.out.println();
        System.out.println("Печать массива через foreach");
        int[] array = new int[] {5, 6, 7, 8, 9};
        for (int el : array) {
            System.out.println(el);
        }

    }

}
