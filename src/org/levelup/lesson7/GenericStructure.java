package org.levelup.lesson7;

// Generic-класс - параметризированный класс
public class GenericStructure<T> {

    // private <unknown type> value;
    private T value;

    public void add(T value) {
        this.value = value;
    }

    public T get() {
        return value;
    }

}
