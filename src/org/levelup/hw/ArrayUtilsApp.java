package org.levelup.hw;

public class ArrayUtilsApp {

    public static void main(String[] args) {
        ArrayUtils utils = new ArrayUtils();

        int[] array = {4, 6, 8, 9};
        int min = utils.min(array);
        int min2 = utils.min(new int[] {45, 67, 8, 86});

        System.out.println(min);
        System.out.println(min2);
    }

}
