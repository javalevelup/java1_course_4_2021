package org.levelup.hw;

public class ArrayUtils {

    // Метод для поиска минимума в массиве
    public int min(int[] array) {
        int min = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
        }
        return min;
    }

}
