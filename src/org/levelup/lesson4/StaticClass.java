package org.levelup.lesson4;

public class StaticClass {

    static int intStaticVariable;
    int intVariable;

    void printIntVariable() {
        System.out.println("Usual: " + intVariable);
        System.out.println("Static: " + intStaticVariable);
    }

    static void printStaticVariable() {
        System.out.println("Only static: " + intStaticVariable);
//        StaticClass sc = new StaticClass();
//        sc.intVariable = 45;
//        sc.printIntVariable();
    }

}
