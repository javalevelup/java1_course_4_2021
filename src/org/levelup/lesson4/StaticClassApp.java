package org.levelup.lesson4;

public class StaticClassApp {

    public static void main(String[] args) {

        StaticClass.intStaticVariable = 780;

        StaticClass sc = new StaticClass();
        sc.intVariable = 540;

        System.out.println(StaticClass.intStaticVariable);

        sc.printIntVariable();
        StaticClass.printStaticVariable();


        String s = "";

        // s.isBlank() "   "
    }

}
