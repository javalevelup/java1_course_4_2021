package org.levelup.lesson4;

public class StringUtils {

    // StringUtils.isBlank(value)
    public static boolean isBlank(String val) {
        return val == null || val.isBlank();
    }

}
