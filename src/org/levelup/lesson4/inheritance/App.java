package org.levelup.lesson4.inheritance;

public class App {

    public static void main(String[] args) {
        EquilateralTriangle et = new EquilateralTriangle(6);
        for (int i = 0; i < et.sizes.length; i++) {
            System.out.println(et.sizes[i]);
        }
        System.out.println("Периметр равностороннего треугольника: " + et.calcPerimeter());

        Shape s = new Shape(new int[] {5, 6, 7, 8, 9});

        Rectangle rec = new Rectangle(4, 5);
        System.out.println("Площадь прямоугольника: " + rec.calcSquare());
        System.out.println("Площадь равностороннего треугольника: " + et.calcSquare());
    }

}
