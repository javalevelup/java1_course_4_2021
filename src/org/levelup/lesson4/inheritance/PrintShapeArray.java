package org.levelup.lesson4.inheritance;

public class PrintShapeArray {

    public static void main(String[] args) {
        // Rectangle[]
        // Shape[]
        // Triangle[]
        // EquilateralTriangle[]

        // void printRectangles(Rectangle[] recs) { for (int i = 0; i < rec.length; i++) sout(recs[i].calcPerimeter()); }
        // void printShape(Shape[] shapes) { for (int i = 0; i < shapes.length; i++) sout(shapes[i].calcPerimeter()); }
        // void printTriangle(Triangle[] trs) { for (int i = 0; i < trs.length; i++) sout(trs[i].calcPerimeter()); }
        // void printEquilateralTriangle(EquilateralTriangle[] etrs) { for (int i = 0; i < etrs.length; i++) sout(etrs[i].calcPerimeter()); }

        Shape[] shapes = new Shape[5];
        shapes[0] = new Shape(new int[] {1, 4, 5, 6, 7});
        // Rectangle r = new Rectangle(6, 7);
        // Shape s = r;
        // shapes[1] = s;
        shapes[1] = new Rectangle(6, 7);
        shapes[2] = new Triangle(4, 5, 6);
        shapes[3] = new EquilateralTriangle(7);
        shapes[4] = new Rectangle(8, 9);

        // printShapePerimeters(shapes);
        printShapeSquare(shapes);


        // Shape s = new Rectangle(5, 6);
        // s.calcSquare() -> выполнится переопределенный метод из класса Rectangle
    }

    static void printShapePerimeters(Shape[] shapes) {
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].calcPerimeter());
        }
    }

    static void printShapeSquare(Shape[] shapes) {
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].calcSquare());
        }
    }

}
