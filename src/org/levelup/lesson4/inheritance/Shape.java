package org.levelup.lesson4.inheritance;

// Класс, от которого унаследовались (Shape) - базовый класс, суперкласс, родительский класс (baseclass, superclass)
// Класс, который наследуют другой класс (Rectangle, Triangle) - подкласс (subclass)
public class Shape {

    protected int[] sizes;

//    public Shape() {
//        this.sizes = new int[0];
//        System.out.println("Shape constructor");
//    }

    public Shape(int[] sizes) {
        this.sizes = sizes;
    }

    public double calcSquare() {
        return 0;
    }

    public double calcPerimeter() {
        double p = 0;
        for (int i = 0; i < sizes.length; i++) {
            p = p + sizes[i];
        }
        return p;
    }

}
