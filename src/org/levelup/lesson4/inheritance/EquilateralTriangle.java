package org.levelup.lesson4.inheritance;

public class EquilateralTriangle extends Triangle {

    public EquilateralTriangle(int a) {
        super(a, a, a);
        System.out.println("EquilateralTriangle constructor");
    }

    public int getSize() {
        return sizes[0];
    }

}
