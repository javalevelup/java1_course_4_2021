package org.levelup.lesson4.inheritance;

public class Triangle extends Shape {

    public Triangle(int a, int b, int c) {
        // new int[] { 4, 5, 6 }; -> [4, 5, 6]
        //
        // int[] ar = new int[3];
        // ar[0] = a;
        // ar[1] = b;
        // ar[2] = c;
        super(new int[] { a, b, c} );
        System.out.println("Triangle constructor");
    }

    @Override
    public double calcSquare() {
        double halfp = calcHalfPerimeter();

        // sqrt(p * (p - a) * (p - b) * (p - c))
        double a = halfp - sizes[0];
        double b = halfp - sizes[1];
        double c = halfp - sizes[2];
        return Math.sqrt(halfp * a * b * c);
    }

    private double calcHalfPerimeter() {
        return calcPerimeter() / 2;
    }

}
