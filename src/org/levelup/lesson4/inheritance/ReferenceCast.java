package org.levelup.lesson4.inheritance;

@SuppressWarnings("ALL")
public class ReferenceCast {

    public static void main(String[] args) {

        EquilateralTriangle et = new EquilateralTriangle(4);
        Triangle tr = et; // вверх по иерархии - расширяющее преобразование (неявное)
        Object ob = tr;

        Shape s = (Shape) ob; // вниз по иерархии - сужающее преобразование (явное)
        EquilateralTriangle e = (EquilateralTriangle) s;
        EquilateralTriangle eqt = (EquilateralTriangle) ob;

        Triangle triangle = new Triangle(4, 5, 6);
        Shape shape = triangle;
        // EquilateralTriangle equilateralTriangle = (EquilateralTriangle) shape; -> ClassCastException

        // Rectange r = new Rectange(4, 5);
        // Shape sh = r;
        Shape sh = new Rectangle(4, 5);

    }

}
