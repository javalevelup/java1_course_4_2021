package org.levelup.lesson4.inheritance;

public class Rectangle extends Shape {

    public Rectangle(int l, int w) {
        super(new int[] { l, w, l, w });
    }

    @Override
    public double calcSquare() {
        // super.<method> - вызов метода из суперкласса
        return sizes[0] * sizes[1];
    }

}
