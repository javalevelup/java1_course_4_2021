package org.levelup.lesson1;

// org.levelup.lesson1.Primitives
public class Primitives {

    // psvm + Tab/Enter
    // sout + Tab/Enter
    public static void main(String[] args) {
        // 00001010 (2) -> 10 (10)

        // <тип переменной> <название(имя) переменной>
        int intVariable; // Объявили переменную
        intVariable = 6533; // Присвоили значение

        int secondVar = 7867; // Инициализация переменной

        int multiply = intVariable * secondVar;
        System.out.println(multiply);

        // "Hello" -> 72 101 108 108 111

        double dVar = 755.34;
        System.out.println("Double value: " + dVar);
        // String + double -> String + String -> String
        // "Double value: " + 755.34 -> "Double value: " + "755.34" -> "Double value: 755.34"

        int a = 10;
        int b = 10;

        // 10 + 40 + 5 + 50 = 50 + 5 + 50 = 55 + 50 = 105
        System.out.println(a + b + " = sum"); // 20 = sum
        System.out.println("sum = " + a + b); // sum = 1010
        System.out.println("sum = " + (a + b));

        // a + b + " = sum"
        // int + int + String = int + String = String
        // 10 + 10 + " = sum" = 20 + " = sum" = "20" + " = sum" = "20 = sum"

        // "sum = " + a + b
        // String + int + int = String + String + int = String + int = String
        // "sum = " + 10 + 10 = "sum = " + "10" + 10 = "sum = 10" + 10 = "sum = 10" + "10" = "sum = 1010"

    }

}
