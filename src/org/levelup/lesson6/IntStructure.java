package org.levelup.lesson6;

// 1. Все методы - public abstract
// 2. Все поля - public static final
// 3. У интерфейса нет конструкторов
// 4.
public interface IntStructure {
    // public static final String FIELD = "";

    void add(int value);

    void removeByValue(int value);

    void print();

}
