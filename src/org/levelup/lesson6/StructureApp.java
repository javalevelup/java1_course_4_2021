package org.levelup.lesson6;

public class StructureApp {

    public static void main(String[] args) {
//        IntList list = new IntList();
//        list.addLast(5);
//        System.out.println("Количество элементов в IntList: " + list.getSize());

        IntList dynamicArray = new DynamicArray();
        System.out.println("Количество элементов в списке: " + dynamicArray.getSize());

        dynamicArray.addLast(4);
        dynamicArray.addLast(7);
        dynamicArray.addLast(10);
        dynamicArray.addLast(50);
        dynamicArray.addLast(9);

        System.out.println("Количество элементов в списке (после добавлений): " + dynamicArray.getSize());


        // DynamicArray dynamicArray = new DynamicArray();
        // IntStructure intStructure = dynamicArray;
        IntStructure intStructure = new DynamicArray();
        intStructure.add(45);
        System.out.println();
    }

}
