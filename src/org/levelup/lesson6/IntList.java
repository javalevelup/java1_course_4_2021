package org.levelup.lesson6;

// Abstract classes
// 1. Нельзя создать объект класса (нельзя вызвать конструктор в паре с оператором new)
// 2. Он может содержать абстрактные методы
//      - абстрактный метод - метод без тела (это только описание/объявление метода)
// Список
public abstract class IntList implements IntStructure {

    protected int size;

    @Override
    public void add(int value) {
        addLast(value);
    }

    public abstract void addFirst(int value);
    public abstract void addLast(int value);

    public int getSize() {
        return size;
    }

}
