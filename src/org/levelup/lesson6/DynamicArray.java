package org.levelup.lesson6;

// "Динамический" массив (список на основе массива)
// Список - набор однотипных элементов
public class DynamicArray extends IntList {

    private int[] elements;
    // private int size;       // размер списка (количество элементов в структуре данных)
                            // индекс, куда необходимо вставить элемент (при addLast)

    public DynamicArray() {
        this.elements = new int[3];
    }

    // [0, 0, 0]
    // addFirst(4) -> [4, 0, 0]
    // addFirst(7) -> [7, 4, 0]
    // addFirst(10) -> [10, 7, 4]
    public void addFirst(int value) {
        if (size == elements.length) {
            int[] oldArray = elements;
            elements = new int[(int)(elements.length * 1.5)];

            // [4, 7, 10, 50] - oldArray
            // [0, 4, 7, 10, 50, 0] - elements
            System.arraycopy(oldArray, 0, elements, 1, oldArray.length);

            elements[0] = value;
            size = size + 1; // -> addFirst(2) -> [2, 4, 7, 10, 50, 0]
        } else {
            System.arraycopy(elements, 0, elements, 1, size);
            elements[0] = value;
            size = size + 1;
        }
    }

    // Добавляем элемент в конец СПИСКА
    // [0, 0, 0]
    // size = 0, addLast(4) ->  [4, 0, 0], size = 1, element.length = 3
    // size = 1, addLast(7) ->  [4, 7, 0], size = 2, element.length = 3
    // size = 2, addLast(10) -> [4, 7, 10], size = 3, element.length = 3
    // size = 3, addLast(50) -> [4, 7, 10, 50], size = 4, element.length = 4
    // size = 4, addLast(2) ->  [4, 7, 10, 50, 2, 0], size = 5, element.length = 6
    @Override
    public void addLast(int value) {
        if (size == elements.length) { // что массив заполнен
            // увеличиваем размер массив
            int[] oldArray = elements;
            elements = new int[(int)(elements.length * 1.5)]; // увеличили размер массива в 1.5
                                                              // создали новый массив с длинной x1.5
            System.arraycopy(oldArray, 0, elements, 0, oldArray.length);
        }
        elements[size++] = value;
//        elements[size] = value;
//        size = size + 1;
    }

    @Override
    public void removeByValue(int value) {

    }

    @Override
    public void print() {

    }

}
