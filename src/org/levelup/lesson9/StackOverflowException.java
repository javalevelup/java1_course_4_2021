package org.levelup.lesson9;

// checked
public class StackOverflowException extends Exception {

    public StackOverflowException(int stackSize) {
        super("Stack is full. Stack size: " + stackSize);
    }

}
