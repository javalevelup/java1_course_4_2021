package org.levelup.lesson9;

public class StackApp {

    public static void main(String[] args) {
        StackArray<String> stringStack = new StackArray<>(3);

        try {
            stringStack.push("str1");
            stringStack.push("str2");
            stringStack.push("str3");
            String val = stringStack.pop();
            stringStack.push("str4");

            System.out.println(val);
        } catch (StackOverflowException | StackEmptyException exc) {
            System.out.println(exc.getMessage());
        }
    }

}
