package org.levelup.lesson9;

public class FactorialRecursive {

    // fact(4)
    //      -> 4 * fact(3)
    //          -> 3 * fact(2)
    //              -> 2 * fact(1)
    //                  -> 1

    public int factorial(int value) {
        if (value <= 1) {
            return 1;
        }
        return value * factorial(value - 1);
    }


}
