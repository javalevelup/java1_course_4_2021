package org.levelup.lesson9;

// unchecked
public class StackEmptyException extends RuntimeException {

    public StackEmptyException() {
        super("Stack is empty");
    }

}
