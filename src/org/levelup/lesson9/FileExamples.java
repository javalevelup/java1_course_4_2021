package org.levelup.lesson9;

import java.io.File;
import java.io.IOException;

public class FileExamples {

    public static void main(String[] args) throws IOException {

        // src: ../text.txt
        // level-up: /level-up/java_1_course_4_2021
        File textFile = new File("text.txt");
        System.out.println("Is exist on FS: " + textFile.exists());

        boolean hasCreated = textFile.createNewFile(); //
        System.out.println("Has it been created: " + hasCreated);

        File srcDir = new File("src/");
        System.out.println("Is exist on FS: " + srcDir.exists());
        System.out.println("Is directory: " + srcDir.isDirectory());
        System.out.println("Is file: " + srcDir.isFile());

    }

}
