package org.levelup.lesson9;

@SuppressWarnings("ALL")
public class TryCatchFinally {

    public static void main(String[] args) {
        System.out.println(returnSomeValue(9));
        System.out.println(returnSomeValue(10));
    }

    static int returnSomeValue(int i) {
        try {
            // System.exit(0);
            if (i == 10) {
                throw new RuntimeException();
            }
            return 1;
        } catch (Exception exc) {
            return 2;
        } finally {
            return 3;
        }
    }

}
