package org.levelup.lesson9;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    // String -> Date
    // "13.03.2021"
    // Date -> long -> 01.01.1970 00:00:00.0 (UTC) Linux epoch
    // -1 -> 31.12.1969 23:59:59.999 (UTC)
    // -1000 -> 31.12.1969 23:59:59.0 (UTC)
    public static Date parseDate(String value) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            System.out.println("Начинаем преобразовывать строку в дату");
            Date date = formatter.parse(value);
            System.out.println("Успешно преобразовали строку в дату");
            return date;
        } catch (ParseException exc) {
            System.out.println("Вы ввели дату в неправильном формате");
            System.out.println(exc.getMessage());
            exc.printStackTrace();
            return new Date(); // new Date() = now()
        }
    }

    public static Date parseDateWithoutHandlingException(String value) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        System.out.println("Начинаем преобразовывать строку в дату");
        Date date = formatter.parse(value);
        System.out.println("Успешно преобразовали строку в дату");
        return date;
    }

    public static void main(String[] args) throws ParseException {
        String value = "eqwrq.09.2021 12:43:23";

        // Thread.run();
        parseDateWithoutHandlingException(value);

        Date date = parseDate(value);
        System.out.println(date.getTime());
    }

}
