package org.levelup.lesson9;

// LIFO - last in, first out
public class StackArray<T> {

    // push
    // pop / peek

    private Object[] elements;
    private int index;

    public StackArray(int stackSize) {
        // T[] elements;
        // this.elements = new T[stackSize]; <- нельзя создать объект Generic-типа
        this.elements = new Object[stackSize];
    }

    // Добавление значения в стек
    public void push(T val) throws StackOverflowException {
        if (index == elements.length) { // стек заполнен (нет места в массиве)
            throw new StackOverflowException(elements.length);
        }
        elements[index++] = val;
    }

    // Получение значения вершины стека
    public T pop() {
        if (index == 0) { // стек пуст
            throw new StackEmptyException();
        }
        //noinspection unchecked
        return (T) elements[--index];
    }

}
