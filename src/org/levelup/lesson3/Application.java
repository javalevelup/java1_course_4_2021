package org.levelup.lesson3;

public class Application {

    public static void main(String[] args) {
        int intVar = 50; // переменная примитвного типа

        // int var;
        // Person p2;
        // System.out.println(var);
        // System.out.println(p2);

        // Переменная ссылочного типа
        // Объект, ссылкой, экземпляр класса (object, reference, instance) - переменная ссылочного типа

        Person p = new Person(); // new + <class_constructor>();
        // p.name = null;
        // System.out.println(p.name);
        // System.out.println(p.name.toUpperCase());
        // String name = p.getName();
        // System.out.println(name.toUpperCase());

        p.age = 18;
        p.weight = 76.5;
        p.name = "Dmitry";

        p.displayPersonInfo();

        String info = p.getPersonInfo();
        System.out.println(info);
        // System.out.println(p.age);
        System.out.println("Конец программы");
    }

}
