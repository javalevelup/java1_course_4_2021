package org.levelup.lesson3;

// Класс = тип переменной
//      Данные
//      Действия
// 16B +  4B + 8B + ... = 32B
public class Person {

    // Переменная класса - поле класса (field)
    public int age;
    double weight;
    String name;

    // Методы
    // <тип возвращаемого значение> <название метода>(<тип арг1> <название арг1>, <тип арг2> <название арг2>,...) {
    //      ... тело метода ...
    // }
    // void - когда метод ничего не возвращает
    void displayPersonInfo() {
        System.out.println(name + ", " + age + ", " + weight);
    }

    String getPersonInfo() {
//        String personInfo = name + ", " + age + ", " + weight;
//        return personInfo;
        return name + ", " + age + ", " + weight;
    }

    void displayInfo() {
        System.out.println(getPersonInfo());
    }

    String getName() {
        return name;
    }

}
