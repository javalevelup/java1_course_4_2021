package org.levelup.lesson3;

// Модификаторы доступа (на класс, на поле, на метод)
// private - доступ только внутри класса
// default-package (private-package) - доступ внутри класса и доступ внутри пакета
// protected - доступ внутри класса, доступ внутри пакета и доступ из класса-наследника
// public - доступ отовсюду
public class Point {

    // <модификатор доступа> <тип поля> <название поля>;
    private int x;
    private int y;
    private String name;

    // Конструктор
    // Конструктор по-умолчанию
    public Point() {
        this(0, 0, null);
    }

    public Point(int x, int y) {
        this(x, y, null);
//        this.x = x;
//        this.y = y;
    }

    public Point(int x, int y, String name) {
        this.x = x;
        this.y = y;
        this.name = name;
    }

    // Метод изменения координат
    // changePoint(int, int)
    public void changePoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void changePoint(long newX, long newY) {
        x = (int) newX;
        y = (int) newY;
    }

    // Сигнатура метода = название метода + список аргументов (последовательность типов аргументов)
    // changePoint(int, int, String)
    public void changePoint(int newX, int newY, String newName) {
        x = newX;
        y = newY;
        name = newName;
    }

    // changePoint(String, int, int)
    public void changePoint(String newName, int newX, int newY) {
        x = newX;
        y = newY;
        name = newName;
    }

    public void displayPoint() {
        // A(5, 4)
        System.out.println(name  + "(" + x + ", " + y + ")");
    }

}


// int m(int a, double b) - m(int, double)

// 1. int m(int b, double a) - no - m(int, double)
// 2. int m(double b, int a) - yes - m(double, int)
// 3. int m(double a, int b) - yes - m(double, int)
// 4. int m(int a, int a) - yes - m(int, int)
// 5. long m(int a, double b) - no - m(int, double)

