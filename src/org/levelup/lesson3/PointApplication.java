package org.levelup.lesson3;

public class PointApplication {

    public static void main(String[] args) {
        Point p1 = new Point();

        p1.changePoint(6, 7);
        p1.displayPoint();
        p1.changePoint(8, 9, "A");
        p1.displayPoint();

        long x = 10;
        long y = 5;
        p1.changePoint(x, y);
        p1.changePoint(12L, 12L);

        p1.changePoint(5, 7L);


        Point p2 = new Point(5, 6, "B");
        p2.changePoint(1, 1);
        p2.displayPoint();
        // int x = 10;
        // int y = 50;
        // p1.changePoint(x, y);
        // p1.changePoint(8, 5);
    }

}
